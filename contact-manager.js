const fs = require("fs").promises;

const dotenv = require("dotenv");
const puppeteer = require("puppeteer");
const _ = require("lodash");
const dateFns = require("date-fns");
const incarceratedPeopleLocator = require("wi-doc-incarcerated-peoples-current-locations");

const { login } = require("./login");
const { timeout } = require("./timeout");
const { logger } = require("./logger");
const { readFile } = require("./read-file");

dotenv.config();

const addNewTempGroup = async (page) => {
  const tempGroupName = `___${Date.now()}`;

  const addGroupLinkSelector =
    "#ctl00_mainContentPlaceHolder_registerInmateLinkButton";
  await page.click(addGroupLinkSelector);

  const newGroupNameInputSelector =
    "#ctl00_mainContentPlaceHolder_GroupNameTextBox";
  await page.waitForSelector(newGroupNameInputSelector);
  await page.type(newGroupNameInputSelector, tempGroupName);

  const newGroupNameSaveButtonSelector =
    "#ctl00_mainContentPlaceHolder_SaveGroupButton";
  await page.click(newGroupNameSaveButtonSelector);

  await page.waitForSelector(addGroupLinkSelector);
  return tempGroupName;
};

const removeGroup = async (page) => {
  const deleteGroupButtonSelector =
    "#ctl00_mainContentPlaceHolder_DeleteGroupButton";

  page.once("dialog", (dialog) => {
    dialog.accept();
  });

  await page.click(deleteGroupButtonSelector);
};

const getAllAvailableContactDocNumbers = async (page) => {
  const allAvailableContactDocNumbers = [];
  let nextPageExists = true;
  let nextPage = 2;

  while (nextPageExists) {
    const docNumbersOnPage = await page.evaluate(() => {
      const contactNameTableCellsSelector =
        "#ctl00_mainContentPlaceHolder_InmateAddGridView > tbody th.MessageDataGrid.Item";
      const contactNameTableCells = document.querySelectorAll(
        contactNameTableCellsSelector
      );
      const docNumbersOnPage = [];

      for (let cell of contactNameTableCells.values()) {
        const docNumber = Number(cell.nextElementSibling.textContent);
        docNumbersOnPage.push(docNumber);
      }

      return docNumbersOnPage;
    });

    allAvailableContactDocNumbers.push(...docNumbersOnPage);

    try {
      await clickNextAvailableContactsPageLink(page, nextPage++);
    } catch (e) {
      if (e === "No next page") {
        nextPageExists = false;
      } else {
        throw e;
      }
    }
  }

  return allAvailableContactDocNumbers;
};

const getAllGroupContacts = async (page) => {
  const allGroupContacts = [];
  let nextPageExists = true;
  let nextPage = 2;

  while (nextPageExists) {
    const groupContactsOnPage = await page.evaluate(() => {
      const groupContactTableRowsSelector =
        "#ctl00_mainContentPlaceHolder_GroupInmateGridView > tbody > tr:not([class])";
      const contactNameTableRows = document.querySelectorAll(
        groupContactTableRowsSelector
      );
      const groupContactsOnPage = [];

      for (let row of contactNameTableRows) {
        groupContactsOnPage.push({
          docNumber: Number(row.querySelector("td:first-of-type").textContent),
          name: row.querySelector("th").textContent,
        });
      }

      return groupContactsOnPage;
    });

    allGroupContacts.push(...groupContactsOnPage);

    try {
      await clickNextGroupContactsPageLink(page, nextPage++);
    } catch (error) {
      if (error === "No next page") {
        logger.info(`Page ${nextPage - 1} is the last page of group contacts`);
        nextPageExists = false;
      } else {
        logger.error(e);
        throw error;
      }
    }
  }

  return allGroupContacts;
};

const findGroupLinkSelector = async (page, group) => {
  const contactGroupLinksSelector =
    "#ctl00_mainContentPlaceHolder_GridViewGroupName td.MessageDataGrid.Item a";
  await page.waitForSelector(contactGroupLinksSelector);

  return await page.evaluate(
    (group, contactGroupLinksSelector) => {
      const links = document.querySelectorAll(contactGroupLinksSelector);

      let groupLinkSelector;

      for (let link of links.values()) {
        if (group === link.textContent.toUpperCase()) {
          groupLinkSelector = `#${link.id}`;
          break;
        }
      }

      return groupLinkSelector;
    },
    group.toUpperCase(),
    contactGroupLinksSelector
  );
};

const clickGroupLink = async (page, group) => {
  let groupLinkSelector = await findGroupLinkSelector(page, group);
  if (!groupLinkSelector) {
    let nextPageNumber = 1;
    while (!groupLinkSelector) {
      nextPageNumber = nextPageNumber + 1;
      const nextPageLinkSelector = `a[href="javascript:__doPostBack('ctl00$mainContentPlaceHolder$GridViewGroupName','Page$${nextPageNumber}')"]`;
      try {
        await page.click(nextPageLinkSelector);
      } catch {
        logger.error(`Group ${group} not found`);
        break;
      }

      const currentPageSpanXPath = `//tr[@class="Pager"]/td/table/tbody/tr/td/span[contains(text(),"${nextPageNumber}")]`;
      await page.waitForXPath(currentPageSpanXPath);
      groupLinkSelector = await findGroupLinkSelector(page, group);
    }
  }

  logger.debug(`Opening group ${group}`);

  await page.click(groupLinkSelector);
  const groupTableSelector = "#ctl00_mainContentPlaceHolder_PanelGroupInmate";
  await page.waitForSelector(groupTableSelector, { timeout: 60000 });
};

const findAddContactLinkSelector = async (page, docNumber) =>
  await page.evaluate((docNumber) => {
    const contactNameTableCellsSelector =
      "#ctl00_mainContentPlaceHolder_InmateAddGridView > tbody th.MessageDataGrid.Item";
    const contactNameTableCells = document.querySelectorAll(
      contactNameTableCellsSelector
    );

    let addContactLinkSelector;

    for (let cell of contactNameTableCells.values()) {
      if (docNumber.toString() === cell.nextElementSibling.textContent) {
        const addContactLink = cell.parentNode.querySelector("a");
        addContactLinkSelector = `#${addContactLink.id}`;
        break;
      }
    }

    return addContactLinkSelector;
  }, docNumber);

const clickNextAvailableContactsPageLink = async (page, nextPageNumber) => {
  const nextPageLinkSelector = `a[href="javascript:__doPostBack('ctl00$mainContentPlaceHolder$InmateAddGridView','Page$${nextPageNumber}')"]`;

  try {
    await page.waitForSelector(nextPageLinkSelector);
  } catch (e) {
    logger.error(e);
    throw "No next page";
  }

  await page.click(nextPageLinkSelector);

  const currentPageSpanXPath = `//div[@id="ctl00_mainContentPlaceHolder_InmateListingPanel"]//tr[@class="Pager"]/td/table/tbody/tr/td/span[contains(text(),"${nextPageNumber}")]`;
  await page.waitForXPath(currentPageSpanXPath);
};

const clickNextGroupContactsPageLink = async (page, nextPageNumber) => {
  const nextPageLinkSelector = `a[href="javascript:__doPostBack('ctl00$mainContentPlaceHolder$GroupInmateGridView','Page$${nextPageNumber}')"]`;

  try {
    await page.waitForSelector(nextPageLinkSelector);
  } catch (e) {
    throw "No next page";
  }

  await page.click(nextPageLinkSelector);

  const currentPageSpanXPath = `//*[@id="ctl00_mainContentPlaceHolder_GroupInmateGridView"]//tr[@class="Pager"]/td/table/tbody/tr/td/span[contains(text(),"${nextPageNumber}")]`;
  await page.waitForXPath(currentPageSpanXPath);
};

const saveChanges = async (page) => {
  const saveGroupChangesButton =
    "#ctl00_mainContentPlaceHolder_SaveGroupButton";
  await page.click(saveGroupChangesButton);
  logger.debug("Changes saved");
  const contactGroupsTableSelector =
    "#ctl00_mainContentPlaceHolder_GridViewGroupName";
  await page.waitForSelector(contactGroupsTableSelector);
};

const getGroupContactCount = async (page) => {
  return await page.evaluate(() => {
    const countMessageSelector =
      "#ctl00_mainContentPlaceHolder_PanelGroupInmate > div";
    const countMessageElement = document.querySelector(countMessageSelector);
    const countMessage = countMessageElement.innerText.trim();
    const regex = /Selected Inmates \((\d+) (inmates|inmate) in this group\)/;
    const countString = countMessage.replace(regex, "$1");

    return Number(countString);
  });
};

const waitForGroupContactToBeAdded = async (page, nextCount) =>
  await page.waitForFunction(
    (nextCount) => {
      const countMessageSelector =
        "#ctl00_mainContentPlaceHolder_PanelGroupInmate > div";
      const countMessageElement = document.querySelector(countMessageSelector);
      const countMessage = countMessageElement.innerText.trim();
      const regex = /Selected Inmates \((\d+) (inmates|inmate) in this group\)/;
      const countString = countMessage.replace(regex, "$1");
      const count = Number(countString);

      return count === nextCount;
    },
    { timeout: 90000 },
    nextCount
  );

const addContactsToGroup = async (
  page,
  contacts,
  group,
  allContactsAdded,
  allContactsNotAdded,
  currentAvailableContactsPageNumber = 1
) => {
  const notAddedContacts = [];
  let searchPageAgain = false;

  for (let contact of contacts) {
    const addContactLinkSelector = await findAddContactLinkSelector(
      page,
      contact.docNumber
    );

    if (addContactLinkSelector) {
      logger.debug(
        `Adding contact w/ DOC # ${contact.docNumber} to group ${group}`
      );
      try {
        const groupContactCount = await getGroupContactCount(page);
        const nextGroupContactCount = groupContactCount + 1;
        await page.click(addContactLinkSelector);
        await waitForGroupContactToBeAdded(page, nextGroupContactCount);
      } catch (exception) {
        logger.warn(
          `Contact w/ DOC # ${contact.docNumber} could not be added to group ${group}`
        );
        throw exception;
      }

      allContactsAdded.push(contact);
      logger.debug(
        `Contact w/ DOC # ${contact.docNumber} was added to group ${group}`
      );
      searchPageAgain = true;
    } else {
      notAddedContacts.push(contact);
    }
  }

  if (notAddedContacts.length > 0) {
    const nextAvailableContactsPageNumber = searchPageAgain
      ? currentAvailableContactsPageNumber
      : currentAvailableContactsPageNumber + 1;

    if (!searchPageAgain) {
      try {
        await clickNextAvailableContactsPageLink(
          page,
          nextAvailableContactsPageNumber
        );
      } catch (error) {
        allContactsNotAdded.push(...notAddedContacts);
        logger.error(
          `${
            allContactsNotAdded.length === 1
              ? "1 contact was"
              : `${allContactsNotAdded.length} contacts were`
          } not added to group ${group}:\n${notAddedContacts
            .map((contact) => contact.docNumber)
            .join("\n")}`
        );
        saveChanges(page);
        return;
      }
    }

    await addContactsToGroup(
      page,
      notAddedContacts,
      group,
      allContactsAdded,
      allContactsNotAdded,
      nextAvailableContactsPageNumber
    );
  } else {
    await saveChanges(page);
  }
};

const addContacts = async function addContacts(
  docNumbers,
  addFromAllContacts,
  groups
) {
  const startTime = Date.now();
  let locatedContacts = [];

  if (groups.length > 0) {
    for (let group of groups) {
      locatedContacts.push(
        ...docNumbers.map((docNumber) => ({ docNumber, location: group }))
      );
    }
  } else {
    const previouslyLocatedContactsFileContents = await fs.readFile(
      "./located-contacts.json"
    );
    const previouslyLocatedContacts = JSON.parse(
      previouslyLocatedContactsFileContents
    );
    const uniqueDocNumbers = _.uniq(docNumbers);
    const uniquePreviouslyLocatedContactsDocNumbers = _.uniq(
      previouslyLocatedContacts.map((contact) => Number(contact.docNumber))
    );
    const docNumbersDifference = _.difference(
      uniqueDocNumbers,
      uniquePreviouslyLocatedContactsDocNumbers
    );
    const previouslyLocatedContactsMatchDocNumbers =
      previouslyLocatedContacts.length > 0 &&
      uniqueDocNumbers.length ===
        uniquePreviouslyLocatedContactsDocNumbers.length &&
      docNumbersDifference.length === 0;

    if (previouslyLocatedContactsMatchDocNumbers) {
      locatedContacts.push(...previouslyLocatedContacts);
    } else {
      try {
        const newlyLocatedContacts =
          await incarceratedPeopleLocator.getCurrentLocations(docNumbers);
        locatedContacts.push(...newlyLocatedContacts);
        await fs.writeFile(
          "./located-contacts.json",
          JSON.stringify(newlyLocatedContacts, null, 2)
        );
      } catch (e) {
        logger.error(e);
        throw e;
      }
    }
  }

  try {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    await page.goto("https://www.corrlinks.com/RegisterGroup.aspx");

    page.on("console", (consoleMessage) => {
      logger[consoleMessage._type](consoleMessage._text);
    });

    await page.setViewport({
      width: 1920,
      height: 800,
    });

    await login(page);
    await timeout(2000);

    const allContactsAdded = [];
    const allContactsNotAdded = [];
    const groupedContacts = _.groupBy(locatedContacts, "location");

    for (let group in groupedContacts) {
      logger.debug("Navigating to Manage My Inmate Groups page");
      await page.goto("https://www.corrlinks.com/RegisterGroup.aspx");
      await clickGroupLink(page, group);

      if (addFromAllContacts) {
        const showAllContactsCheckboxSelector =
          "#ctl00_mainContentPlaceHolder_selectNewInmatesCheckBox";
        await page.waitForSelector(showAllContactsCheckboxSelector);
        await page.click(showAllContactsCheckboxSelector);
      }

      await timeout(2000);
      await addContactsToGroup(
        page,
        groupedContacts[group],
        group,
        allContactsAdded,
        allContactsNotAdded
      );
    }

    await fs.writeFile(
      "./not-added.json",
      JSON.stringify(allContactsNotAdded, null, 2)
    );
    await fs.writeFile(
      "./added.json",
      JSON.stringify(allContactsAdded, null, 2)
    );
    const endTime = Date.now();
    const elapsedTime = dateFns.formatDistanceStrict(startTime, endTime);
    logger.info(`Ran for ${elapsedTime}`);
    await browser.close();
    process.exit();
  } catch (error) {
    logger.error(error);
    if (
      _.isArray(error) &&
      error.every((item) => item === "CAPCHA_NOT_READY")
    ) {
      addContacts(docNumbers, addFromAllContacts, groups);
    }
    process.exit(1);
  }
};

exports.addContacts = addContacts;

exports.addNewContacts = async function addNewContacts() {
  const startTime = Date.now();
  let docNumbers = [];

  try {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    await page.goto("https://www.corrlinks.com/RegisterGroup.aspx");

    page.on("console", (consoleMessage) => {
      logger[consoleMessage._type](consoleMessage._text);
    });

    await page.setViewport({
      width: 1920,
      height: 800,
    });

    await login(page);

    logger.debug("Navigating to Manage My Inmate Groups page");
    await page.goto("https://www.corrlinks.com/RegisterGroup.aspx");
    const newTempGroupName = await addNewTempGroup(page);
    await clickGroupLink(page, newTempGroupName);
    docNumbers.push(...(await getAllAvailableContactDocNumbers(page)));
    await removeGroup(page, newTempGroupName);
  } catch (e) {
    logger.error(e);
    throw e;
  }

  await addContacts(docNumbers, false, []);
  await addContacts(docNumbers, true, ["_IWOC Contacts 14"]);

  const endTime = Date.now();
  const elapsedTime = dateFns.formatDistanceStrict(startTime, endTime);
  logger.info(`Ran for ${elapsedTime}`);
};

exports.getContactsInGroup = async function getContactsInGroup(group) {
  try {
    const startTime = Date.now();
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    await page.goto("https://www.corrlinks.com/RegisterGroup.aspx");

    page.on("console", (consoleMessage) => {
      logger[consoleMessage._type](consoleMessage._text);
    });

    await page.setViewport({
      width: 1920,
      height: 800,
    });

    await login(page);
    await timeout(2000);

    logger.debug("Navigating to Manage My Inmate Groups page");
    await page.goto("https://www.corrlinks.com/RegisterGroup.aspx");
    await clickGroupLink(page, group);
    await timeout(2000);

    const allGroupContacts = await getAllGroupContacts(page);

    await fs.writeFile(
      "./group-contacts.json",
      JSON.stringify(allGroupContacts, null, 2)
    );

    const endTime = Date.now();
    const elapsedTime = dateFns.formatDistanceStrict(startTime, endTime);
    logger.info(`Ran for ${elapsedTime}`);
    await browser.close();
    process.exit();
  } catch (error) {
    logger.error(error);
    if (
      _.isArray(error) &&
      error.every((item) => item === "CAPCHA_NOT_READY")
    ) {
      await getContactsInGroup(group);
    }
    process.exit(1);
  }
};
