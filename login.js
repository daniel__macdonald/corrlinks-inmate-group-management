const { getCaptchaBypassToken } = require("./bypass-captcha.js");
const { logger } = require("./logger.js");

exports.login = async function login(page) {
  await page.goto(
    "https://www.corrlinks.com/Login.aspx?ReturnUrl=%2fRegisterGroup.aspx"
  );
  const defaultWaitForOptions = { timeout: 5000 };

  const loginEmailAddressInputSelector =
    "#ctl00_mainContentPlaceHolder_loginUserNameTextBox";
  const loginPasswordInputSelector =
    "#ctl00_mainContentPlaceHolder_loginPasswordTextBox";
  const loginSubmitButtonSelector = "#ctl00_mainContentPlaceHolder_loginButton";
  await page.waitForSelector(
    loginEmailAddressInputSelector,
    defaultWaitForOptions
  );
  logger.debug("Logging in");
  await page.type(
    loginEmailAddressInputSelector,
    process.env.LOGIN_EMAIL_ADDRESS
  );
  await page.type(loginPasswordInputSelector, process.env.LOGIN_PASSWORD);
  await page.click(loginSubmitButtonSelector);

  const recaptchaSelector = "#ctl00_mainContentPlaceHolder_captchaDiv";
  await page.waitForSelector(recaptchaSelector, defaultWaitForOptions);
  const siteKey = await page.evaluate(
    (recaptchaSelector) =>
      document.querySelector(recaptchaSelector).dataset.sitekey,
    recaptchaSelector
  );
  const apiKey = process.env["2CAPTCHA_API_KEY"];
  const params = {
    method: "hcaptcha",
    sitekey: siteKey,
    key: apiKey,
    pageurl: page.url(),
    json: 1,
  };
  const token = await getCaptchaBypassToken(apiKey, params);

  logger.debug("Bypassing captcha");
  await page.evaluate(
    (token, recaptchaSelector) => {
      document.querySelector('[name="g-recaptcha-response"]').value = token;
      document.querySelector('[name="h-captcha-response"]').value = token;
      const { callback } = document.querySelector(recaptchaSelector).dataset;
      console.debug("Recaptcha callback name: ", callback);
      window[callback]();
    },
    token,
    recaptchaSelector
  );

  const proceedAfterRecaptchaSubmitButtonSelector =
    "#ctl00_mainContentPlaceHolder_captchaFNameLNameSubmitButton:not(:disabled)";
  await page.waitForSelector(proceedAfterRecaptchaSubmitButtonSelector, {
    timeout: 100000,
  });
  await page.click(proceedAfterRecaptchaSubmitButtonSelector);
};
