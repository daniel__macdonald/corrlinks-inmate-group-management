export const retry = (fn, maxRetries = 3) => {
  return fn().catch(function (err) {
    if (maxRetries <= 0) {
      throw err;
    }
    return retry(async () => fn, maxRetries - 1);
  });
};
