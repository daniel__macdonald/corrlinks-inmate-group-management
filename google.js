const googleapis = require("googleapis");
const _ = require("lodash");

const google = new googleapis.GoogleApis();

function authorize() {
  const clientId = process.env.GOOGLE_AUTH_CLIENT_ID;
  const clientSecret = process.env.GOOGLE_AUTH_CLIENT_SECRET;
  const redirectUri = process.env.GOOGLE_AUTH_REDIRECT_URI;
  const token = {
    access_token: process.env.GOOGLE_AUTH_ACCESS_TOKEN,
    refresh_token: process.env.GOOGLE_AUTH_REFRESH_TOKEN,
    scope: process.env.GOOGLE_AUTH_SCOPE,
    token_type: process.env.GOOGLE_AUTH_TOKEN_TYPE,
    expiry_date: process.env.GOOGLE_AUTH_EXPIRY_DATE,
  };
  const oAuth2Client = new google.auth.OAuth2(
    clientId,
    clientSecret,
    redirectUri
  );
  oAuth2Client.setCredentials(token);
  return oAuth2Client;
}

exports.updateDoc = async function updateDoc(documentId, requests) {
  const auth = authorize();
  const docs = google.docs({
    version: "v1",
    auth,
  });

  await docs.documents.batchUpdate({
    auth,
    documentId,
    requestBody: { requests },
  });
}

async function getDoc(documentId) {
  const auth = authorize();
  const docs = google.docs({
    version: "v1",
    auth,
  });

  return await docs.documents.get({
    auth,
    documentId,
  });
}

exports.getDocUpdateStartIndex = async function getDocUpdateStartIndex(documentId) {
  const doc = await getDoc(documentId);
  const { endIndex } = _.last(doc.data.body.content);

  // Update start index must be less than the last end index
  return endIndex - 1;
}
