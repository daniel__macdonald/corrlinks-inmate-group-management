#!/usr/bin/env node

const yargs = require("yargs/yargs")(process.argv.slice(2));
const dateFns = require("date-fns");

const {
  addContacts,
  addNewContacts,
  getContactsInGroup,
} = require("../contact-manager");
const { downloadEmails } = require("../email-manager");
const { logger } = require("../logger");

yargs
  .scriptName("cigm")
  .usage("$0 <cmd> [args]")
  .command(
    "add-contacts",
    "add contacts to their groups",
    (yargs) => {
      yargs.option("all", {
        alias: "a",
        default: false,
        describe: "Add contacts that have already been added to a group",
        type: "boolean",
      });

      yargs.option("groups", {
        alias: "g",
        default: [],
        describe: "Groups to add all contacts to",
        type: "array",
      });
    },
    async ({ all, groups }) => {
      logger.debug(`all: ${all}`);
      logger.debug(`groups: ${groups}`);

      const { readFile } = require("../read-file.js");
      const docNumbers = await readFile("./contacts.txt");

      logger.debug(
        `Adding ${docNumbers.length} contact${docNumbers.length > 1 ? "s" : ""}`
      );

      // await addContacts(docNumbers, all, groups);
    }
  )
  // TODO: enable this command once `addNewContacts` is operational
  // .command("add-new-contacts", "add new contacts to their groups", async () => {
  //   await addNewContacts();
  // })
  .command(
    "get-contacts",
    "get contacts in group",
    (yargs) => {
      yargs.options({
        group: {
          alias: "g",
          describe: "Group to get contacts from",
          type: "string",
          demandOption: true,
        },
      });
    },
    async ({ group }) => {
      logger.debug(`group: ${group}`);
      await getContactsInGroup(group);
    }
  )
  .command(
    "download-emails",
    "download emails",
    (yargs) => {
      yargs.option("startDate", {
        alias: "s",
        default: null,
        description: "Start date (MM/DD/YYYY)",
        type: "string",
      });
      yargs.option("endDate", {
        alias: "e",
        default: dateFns.format(new Date(), "MM/dd/yyyy"),
        description: "End date (MM/DD/YYYY)",
        type: "string",
      });
      yargs.option("numberOfDays", {
        alias: "n",
        default: -1,
        description: "Number of days to scan from the start date",
        type: "number",
      });
      yargs.option("subject", {
        default: "",
        description: "Filter emails by subject",
        type: "string",
      });
      yargs.option("readOnly", {
        alias: "r",
        default: true,
        description: "Only download previously read emails",
        type: "boolean",
      });
      yargs.option("sent", {
        default: false,
        description: "Download sent emails",
        type: "boolean",
      });
    },
    async ({ startDate, endDate, numberOfDays, subject, readOnly, sent }) => {
      logger.debug(`startDate: ${startDate}`);
      logger.debug(`endDate: ${endDate}`);
      logger.debug(`numberOfDays: ${numberOfDays}`);
      logger.debug(`subject: ${subject}`);
      logger.debug(`readOnly: ${readOnly}`);
      logger.debug(`sent: ${sent}`);

      if (numberOfDays >= 0) {
        const parsedEndDate = dateFns.parse(endDate, "MM/dd/yyyy", new Date());
        const unformattedStartDate = dateFns.subDays(
          parsedEndDate,
          numberOfDays
        );
        startDate = dateFns.format(unformattedStartDate, "MM/dd/yyyy");
        logger.debug(`startDate: ${startDate}`);
      }

      await downloadEmails({ startDate, endDate, readOnly, subject, sent });
    }
  )
  .demandCommand(
    1,
    1,
    "1 command must be provided",
    "1 command must be provided"
  )
  .help().argv;
