const bent = require("bent");
const promisePoller = require("promise-poller");

const { logger } = require("./logger");
const { timeout } = require("./timeout");

const { default: poll } = promisePoller;

exports.getCaptchaBypassToken = async function getCaptchaBypassToken(
  apiKey,
  params
) {
  logger.debug("Requesting captcha bypass from 2Captcha...");
  const requestId = await initiateCaptchaRequest(params);
  logger.debug("2Captcha request ID received");

  logger.debug("Polling 2Captcha for results...");
  const token = await pollForRequestResults(apiKey, requestId);
  logger.debug("2Captcha bypass token received");

  return token;
};

const initiateCaptchaRequest = async (params) => {
  const post = bent("POST", "json");
  const response = await post("https://2captcha.com/in.php", params);
  return response.request;
};

const pollForRequestResults = async (
  key,
  id,
  retries = 50,
  interval = 1500,
  delay = 15000
) => {
  await timeout(delay);
  return poll({
    taskFn: requestCaptchaResults(key, id),
    interval,
    retries,
  });
};

const requestCaptchaResults = (apiKey, requestId) => {
  const url = `http://2captcha.com/res.php?key=${apiKey}&action=get&id=${requestId}&json=1`;
  return async () =>
    new Promise(async (resolve, reject) => {
      try {
        const get = bent("GET", "json");
        const response = await get(url);

        if (response.status === 0) {
          reject(response.request);
        } else {
          resolve(response.request);
        }
      } catch (e) {
        logger.error(`2captcha request failed: ${e}`);
        requestCaptchaResults(apiKey, requestId);
      }
    });
};
