const fs = require("fs").promises;

const partialParseInt = (numberString) => parseInt(numberString, 10);

exports.readFile = async function readFile(filePath) {
  const fileContents = await fs.readFile(filePath);
  let docNumbers;

  docNumbers = fileContents.toString().split("\n").map(partialParseInt);

  if (docNumbers.length === 0) {
    throw new Error(
      'Please add a line-separated list of DOC numbers to the contacts.txt file. Please see the README\'s "Data setup" section for an example.'
    );
  }

  return docNumbers;
};
