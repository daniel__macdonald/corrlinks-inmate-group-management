const winston = require("winston");
require("winston-daily-rotate-file");

const { createLogger, format, transports } = winston;
const { combine, timestamp, printf } = format;

const humanReadableFormat = printf(
  ({ level, message, timestamp }) => `${timestamp} ${level}: ${message}`
);

exports.logger = createLogger({
  level: "debug",
  format: combine(timestamp(), humanReadableFormat),
  transports: [
    new transports.Console(),
    new transports.DailyRotateFile({
      filename: "logs/application-%DATE%.log",
      datePattern: "YYYY-MM-DD",
      zippedArchive: true,
      maxSize: "20m",
      maxFiles: "14d",
    }),
  ],
});
