# Managing contacts and emails in Corrlinks

This is a Node command-line interface (CLI) application that will manage Corrlinks contacts and emails.

## Installation

Clone this repository and then install the app's dependencies by running the following commands:

```bash
git clone https://daniel__macdonald@bitbucket.org/daniel__macdonald/corrlinks-inmate-group-management.git
cd corrlinks-inmate-group-management
npm i
```

## Setup

Link the CLI application as a global command by running the command:

```bash
npm link
```

Create a `.env` file and copy the contents of the `.env.example` file to it. Then replace the values with your own so that the CLI application is able to log in to Corrlinks, bypass the captcha, and upload data to Google Docs.

## Commands

The CLI application is run by entering commands.

### `add-contacts`

This command will add contacts to a Corrlinks group:

```bash
cigm add-contacts [options]
```

This command depends on a list of line-separated DOC numbers having been added to the `contacts.txt` file. For example:

```
100001
100002
100003
```

#### Options

The `add-contacts` command also accepts options:

| Name     | Required | Alias | Default value | Accepted values               | Description                                                                                                                                                                                                                                    |
| -------- | -------- | ----- | ------------- | ----------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `all`    | No       | `a`   | `false`       | `true`, `false`               | Contacts will be added even if they have already been previously been added to a group.                                                                                                                                                        |
| `groups` | No       | `g`   |               | The name of a Corrlinks group | If a group is provided, contacts will be added to that group. If no group is provided, contacts will be added to a group with a name that matches their location according to the [WI DOC Offender Locator site](https://appsdoc.wi.gov/lop/). |

An example using options:

```bash
cigm add-contacts --all true --group "Group 1"
```

### `get-contacts`

This command will add contacts to a Corrlinks group:

```bash
cigm get-contacts [options]
```

#### Options

The `get-contacts` command also requires one option:

```bash
cigm get-contacts --group "Group 1"
```

| Name    | Required | Alias | Default value | Accepted values               | Description                                                                                             |
| ------- | -------- | ----- | ------------- | ----------------------------- | ------------------------------------------------------------------------------------------------------- |
| `group` | Yes      | `g`   |               | The name of a Corrlinks group | Retrieves all contacts from the provided group and outputs them to `group-contacts.json` in JSON format |

An example of the output:

```json
[
  {
    "docNumber": 100001,
    "name": "LITTLE, FRANK"
  },
  {
    "docNumber": 100002,
    "name": "HAYWOOD, BILL"
  }
]
```

### `download-emails`

This command will download emails:

```bash
cigm download-emails [options]
```

#### Options

The `download-emails` command also requires one option:

```bash
cigm download-emails --startDate "05/08/2021" --endDate "05/09/20201" --subject "stimulus" --readOnly true
```

| Name           | Required | Alias | Default value                           | Accepted values                                                                        | Description                                                                                                                                                                                        |
| -------------- | -------- | ----- | --------------------------------------- | -------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `startDate`    | No       | `s`   |                                         | Date in `MM/dd/yyyy` format, the same date or earlier than `endDate` (e.g. 05/08/2021) | The start date of the date range to filter emails by. Used in conjuction with `endDate`. If not set, Corrlinks defaults to two weeks before the current date. Overridden by `numberOfDays` if set. |
| `endDate`      | No       | `e`   | The current date in `MM/dd/yyyy` format | Date in `MM/dd/yyyy` format, the same date or later than `startDate` (e.g. 05/09/2021) | The end date of the date range to filter emails by. Used in conjuction with `startDate`. Overridden by `numberOfDays` if set.                                                                      |
| `numberOfDays` | No       | `n`   | `-1`                                    | Any number                                                                             | The number of days to retrieve emails for. If the value is 0 or greater, emails will be filtered for that many days from the current date into the past.                                           |
| `subject`      | No       |       |                                         | Any characters                                                                         | The subject by which to filter emails                                                                                                                                                              |
| `readOnly`     | No       | `r`   | `true`                                  | `true`, `false`                                                                        | A flag to determine whether to only download emails that have already been read                                                                                                                    |
| `sent`         | No       |       | `false`                                 | `true`, `false`                                                                        | A flag to determine whether to only download emails that have already been sent from your account                                                                                                  |

An example of the output:

```json
{
  "5/13/21": [
    {
      "emailId": 200003,
      "unread": false,
      "name": "BILL HAYWOOD",
      "docNumber": 100002,
      "from": "BILL HAYWOOD (100002)",
      "date": "5/13/2021 6:00:52 PM",
      "subject": "Hello",
      "message": "This is an example message."
    }
  ],
  "5/12/21": [
    {
      "emailId": 200002,
      "unread": false,
      "name": "FRANK LITTLE",
      "docNumber": 100001,
      "from": "FRANK LITTLE (100001)",
      "date": "5/12/2021 10:22:01 AM",
      "subject": "Hello again",
      "message": "This is another example message."
    },
    {
      "emailId": 200001,
      "unread": false,
      "name": "FRANK LITTLE",
      "docNumber": 100001,
      "from": "FRANK LITTLE (100001)",
      "date": "5/12/2021 8:04:33 AM",
      "subject": "Hello",
      "message": "This is an example message."
    }
  ]
}
```
