const fs = require("fs").promises;

const dotenv = require("dotenv");
const puppeteer = require("puppeteer");
const dateFns = require("date-fns");

const { logger } = require("./logger");
const { login } = require("./login");
const { timeout } = require("./timeout");
const { updateDoc, getDocUpdateStartIndex } = require("./google");

dotenv.config();

const clickNextInboxPageLink = async (page, nextPageNumber, sent) => {
  const nextPageLinkSelector = sent
    ? `a[href="javascript:__doPostBack('ctl00$mainContentPlaceHolder$messageGridView','Page$${nextPageNumber}')"]`
    : `a[href="javascript:__doPostBack('ctl00$mainContentPlaceHolder$inboxGridView','Page$${nextPageNumber}')"]`;
  await page.waitForSelector(nextPageLinkSelector, { timeout: 500 });
  await page.click(nextPageLinkSelector, { timeout: 500 });

  const currentPageSpanXPath = sent
    ? `//*[@id="ctl00_mainContentPlaceHolder_messageGridView"]/tbody/tr[@class="Pager"]/td/table/tbody/tr/td/span[contains(text(), "${nextPageNumber}")]`
    : `//*[@id="ctl00_mainContentPlaceHolder_inboxGridView"]/tbody/tr[@class="Pager"]/td/table/tbody/tr/td/span[contains(text(), "${nextPageNumber}")]`;
  await page.waitForXPath(currentPageSpanXPath);
};

const filterEmailsByDates = async (page, startDate, endDate) => {
  const starDateInputSelector =
    "#ctl00_mainContentPlaceHolder_startDateTextBox";
  const updateButtonSelector = "#ctl00_mainContentPlaceHolder_updateButton";

  await page.waitForSelector(starDateInputSelector);
  await page.evaluate(
    (starDateInputSelector, startDate, endDate) => {
      const endDateInputSelector =
        "#ctl00_mainContentPlaceHolder_endDateTextBox";

      document.querySelector(starDateInputSelector).value = startDate;
      document.querySelector(endDateInputSelector).value = endDate;
    },
    starDateInputSelector,
    startDate,
    endDate
  );

  await page.click(updateButtonSelector);
  await timeout(2000);
};

const getEmailContentsFromPage = async (page, readOnly, subjectToFilterBy) => {
  const emailIds = await page.evaluate(() => {
    const emailRowsSelector =
      "tr[onmouseover=\"this.className='MessageDataGrid ItemHighlighted'\"]";
    const emailRows = document.querySelectorAll(emailRowsSelector);
    const emailIds = [];

    for (let row of emailRows) {
      const emailId = row.getAttribute("messageid");
      emailIds.push(Number(emailId));
    }

    return emailIds;
  });
  const groupedEmails = { count: 0 };
  Object.defineProperty(groupedEmails, "count", { enumerable: false });

  logger.debug(`Downloading emails ${emailIds.join(", ")}`);

  for (let emailId of emailIds) {
    const rowSelector = `tr[messageid="${emailId}"]`;
    let unread;
    let subject;
    try {
      unread = await page.evaluate((rowSelector) => {
        const row = document.querySelector(rowSelector);
        return row.children[1].classList.contains("BoldItem");
      }, rowSelector);
      subject = await page.evaluate((rowSelector) => {
        const row = document.querySelector(rowSelector);
        return row.children[2].querySelector("span").textContent;
      }, rowSelector);
    } catch (e) {
      console.error(e);
    }

    logger.debug(
      `Email ${emailId} was previously ${unread ? "unread" : "read"}`
    );

    if (
      (readOnly && unread) ||
      (subjectToFilterBy !== "" &&
        !subject.toLowerCase().includes(subjectToFilterBy.toLowerCase()))
    ) {
      continue;
    }

    await page.click(rowSelector);

    const fromSelector = "#ctl00_mainContentPlaceHolder_fromTextBox";
    await page.waitForSelector(fromSelector);
    const from = await page.$eval(fromSelector, (node) => node.value);

    const regexp = /(.+)\s\((\d+)\)/;
    const [name, docNumber] = from.replace(regexp, "$1;$2").split(";");

    const dateSelector = "#ctl00_mainContentPlaceHolder_dateTextBox";
    const dateTime = await page.$eval(dateSelector, (node) => node.value);
    const parsedDate = dateFns.parse(
      dateTime,
      "M/d/yyyy h:mm:ss a",
      new Date()
    );
    const date = dateFns.format(parsedDate, "M/d/yy");

    const messageSelector = "#ctl00_mainContentPlaceHolder_messageTextBox";
    const message = await page.$eval(messageSelector, (node) => node.value);

    if (!Array.isArray(groupedEmails[date])) {
      groupedEmails[date] = [];
    }

    groupedEmails[date].push({
      emailId,
      unread,
      name,
      docNumber: Number(docNumber),
      from,
      date: dateTime,
      subject,
      message,
    });

    groupedEmails.count = groupedEmails.count + 1;

    logger.debug(
      `Downloaded email with subject "${subject}" from ${from} sent on ${dateTime}`
    );

    const closeButtonSelector = "#ctl00_mainContentPlaceHolder_cancelButton";
    await page.click(closeButtonSelector);
    const emailsPanelSelector =
      "#ctl00_mainContentPlaceHolder_MessagesViewPanel";
    await page.waitForSelector(emailsPanelSelector);
  }

  return groupedEmails;
};

const getSentEmailContentsFromPage = async (
  page,
  readOnly,
  subjectToFilterBy
) => {
  const emailIds = await page.evaluate(() => {
    const emailRowsSelector =
      "tr[onmouseover=\"this.className='MessageDataGrid ItemHighlighted'\"]";
    const emailRows = document.querySelectorAll(emailRowsSelector);
    const emailIds = [];

    for (let row of emailRows) {
      const emailId = row.getAttribute("messagegroupid");
      emailIds.push(Number(emailId));
    }

    return emailIds;
  });
  const groupedEmails = { count: 0 };
  Object.defineProperty(groupedEmails, "count", { enumerable: false });

  logger.debug(`Downloading emails ${emailIds.join(", ")}`);

  for (let emailId of emailIds) {
    const rowSelector = `tr[messagegroupid="${emailId}"]`;
    let unread;
    let subject;
    try {
      unread = await page.evaluate((rowSelector) => {
        const row = document.querySelector(rowSelector);
        return row.children[1].classList.contains("BoldItem");
      }, rowSelector);
      subject = await page.evaluate((rowSelector) => {
        const row = document.querySelector(rowSelector);
        return row.children[2].querySelector("span").textContent;
      }, rowSelector);
    } catch (e) {
      console.error(e);
    }

    logger.debug(
      `Email ${emailId} was previously ${unread ? "unread" : "read"}`
    );

    if (
      (readOnly && unread) ||
      (subjectToFilterBy !== "" &&
        !subject.toLowerCase().includes(subjectToFilterBy.toLowerCase()))
    ) {
      continue;
    }

    await page.click(rowSelector);

    const toSelector = "#ctl00_mainContentPlaceHolder_formGridView";
    await page.waitForSelector(toSelector);
    const to = await page.$eval(toSelector, (node) => node.textContent.trim());

    const regexp = /(.+)\s(\d+)/;
    const [name, docNumber] = to.replace(regexp, "$1;$2").split(";");

    const dateSelector = "#ctl00_mainContentPlaceHolder_dateTextBox";
    const dateTime = await page.$eval(dateSelector, (node) => node.value);
    const parsedDate = dateFns.parse(
      dateTime,
      "M/d/yyyy h:mm:ss a",
      new Date()
    );
    const date = dateFns.format(parsedDate, "M/d/yy");

    const messageSelector = "#ctl00_mainContentPlaceHolder_messageTextBox";
    const message = await page.$eval(messageSelector, (node) => node.value);

    if (!Array.isArray(groupedEmails[date])) {
      groupedEmails[date] = [];
    }

    groupedEmails[date].push({
      emailId,
      unread,
      name,
      docNumber: Number(docNumber),
      to,
      date: dateTime,
      subject,
      message,
    });

    groupedEmails.count = groupedEmails.count + 1;

    logger.debug(
      `Downloaded email with subject "${subject}" sent to ${to} on ${dateTime}`
    );

    const closeButtonSelector = "#ctl00_mainContentPlaceHolder_cancelButton";
    await page.click(closeButtonSelector);
    const emailsPanelSelector = "#ctl00_mainContentPlaceHolder_messageGridView";
    await page.waitForSelector(emailsPanelSelector);
  }

  return groupedEmails;
};

const updateGoogleDoc = async (documentId, sentDate, emails) => {
  logger.debug(`Updating doc ${documentId}`);
  let startIndex = await getDocUpdateStartIndex(documentId);
  let endIndex;
  const pageBreakRequest = {
    insertPageBreak: {
      location: {
        index: startIndex,
      },
    },
  };

  const newEmailsText = `EMAILS RECEIVED ON ${sentDate}\n----------------------------------------------------------------------------------------------------\n\n`;
  const pageBreakCharacterLength = 1;
  startIndex = startIndex + pageBreakCharacterLength;
  endIndex = startIndex + newEmailsText.length;
  const newEmailsHeaderRequests = [
    {
      insertText: {
        location: {
          index: startIndex,
        },
        text: newEmailsText,
      },
    },
    {
      updateTextStyle: {
        textStyle: {
          bold: true,
          fontSize: {
            magnitude: 14,
            unit: "PT",
          },
        },
        range: { startIndex, endIndex },
        fields: "bold,fontSize",
      },
    },
    {
      updateParagraphStyle: {
        paragraphStyle: { alignment: "CENTER" },
        range: { startIndex, endIndex },
        fields: "alignment",
      },
    },
  ];
  const requests = [pageBreakRequest, ...newEmailsHeaderRequests];

  for (let email of emails) {
    const newLineCharacterLength = 1;

    let unreadTagRequests = [];
    if (email.unread) {
      const unreadTagText = "[UNREAD]\n";
      startIndex = endIndex;
      endIndex = startIndex + unreadTagText.length;
      unreadTagRequests = [
        {
          insertText: {
            location: {
              index: startIndex,
            },
            text: unreadTagText,
          },
        },
        {
          updateTextStyle: {
            textStyle: {
              backgroundColor: {
                color: {
                  rgbColor: {
                    red: 1,
                    green: 1,
                    blue: 0,
                  },
                },
              },
              bold: true,
              fontSize: {
                magnitude: 11,
                unit: "PT",
              },
            },
            range: {
              startIndex,
              endIndex,
            },
            fields: "backgroundColor,bold,fontSize",
          },
        },
        {
          updateParagraphStyle: {
            paragraphStyle: {
              alignment: "START",
              indentFirstLine: {
                magnitude: 0,
                unit: "PT",
              },
              indentStart: {
                magnitude: 0,
                unit: "PT",
              },
            },
            range: {
              startIndex,
              endIndex,
            },
            fields: "alignment,indentFirstLine,indentStart",
          },
        },
      ];
    }

    const fromLabelText = "From: ";
    startIndex = endIndex;
    endIndex = startIndex + fromLabelText.length;
    const fromLabelRequests = [
      {
        insertText: {
          location: {
            index: startIndex,
          },
          text: fromLabelText,
        },
      },
      {
        updateTextStyle: {
          textStyle: {
            backgroundColor: {
              color: {
                rgbColor: {
                  red: 1,
                  green: 1,
                  blue: 1,
                },
              },
            },
            bold: false,
            fontSize: {
              magnitude: 11,
              unit: "PT",
            },
          },
          range: {
            startIndex,
            endIndex,
          },
          fields: "backgroundColor,bold,fontSize",
        },
      },
      {
        updateParagraphStyle: {
          paragraphStyle: {
            alignment: "START",
            indentFirstLine: {
              magnitude: 0,
              unit: "PT",
            },
            indentStart: {
              magnitude: 0,
              unit: "PT",
            },
          },
          range: {
            startIndex,
            endIndex,
          },
          fields: "alignment,indentFirstLine,indentStart",
        },
      },
    ];

    startIndex = endIndex;
    endIndex = startIndex + email.from.length;
    const fromContentRequests = [
      {
        insertText: {
          location: {
            index: startIndex,
          },
          text: email.from,
        },
      },
      {
        updateTextStyle: {
          textStyle: { bold: true },
          range: { startIndex, endIndex },
          fields: "bold",
        },
      },
    ];

    const dateLabelText = "\nDate: ";
    startIndex = endIndex;
    endIndex = startIndex + dateLabelText.length;
    const dateLabelRequests = [
      {
        insertText: {
          location: {
            index: startIndex,
          },
          text: dateLabelText,
        },
      },
      {
        updateTextStyle: {
          textStyle: {
            bold: false,
          },
          range: {
            startIndex: startIndex + newLineCharacterLength,
            endIndex,
          },
          fields: "bold",
        },
      },
    ];

    startIndex = endIndex;
    endIndex = startIndex + email.date.length;
    const dateContentRequests = [
      {
        insertText: {
          location: {
            index: startIndex,
          },
          text: email.date,
        },
      },
      {
        updateTextStyle: {
          textStyle: { bold: true },
          range: { startIndex, endIndex },
          fields: "bold",
        },
      },
    ];

    const subjectLabelText = "\nSubject: ";
    startIndex = endIndex;
    endIndex = startIndex + subjectLabelText.length;
    const subjectLabelRequests = [
      {
        insertText: {
          location: {
            index: startIndex,
          },
          text: subjectLabelText,
        },
      },
      {
        updateTextStyle: {
          textStyle: {
            bold: false,
          },
          range: {
            startIndex: startIndex + newLineCharacterLength,
            endIndex,
          },
          fields: "bold",
        },
      },
    ];

    startIndex = endIndex;
    endIndex = startIndex + email.subject.length;
    const subjectContentRequests = [
      {
        insertText: {
          location: {
            index: startIndex,
          },
          text: email.subject,
        },
      },
      {
        updateTextStyle: {
          textStyle: { bold: true },
          range: { startIndex, endIndex },
          fields: "bold",
        },
      },
    ];

    startIndex = endIndex;
    endIndex = startIndex + email.message.length + newLineCharacterLength * 2;
    let messageText = `\n\n${email.message}`;
    const isLastEmail = email.emailId === emails[emails.length - 1].emailId;

    if (!isLastEmail) {
      endIndex = endIndex + newLineCharacterLength * 4;
      messageText = `${messageText}\n\n\n\n`;
    }

    const messageContentRequests = [
      {
        insertText: {
          location: {
            index: startIndex,
          },
          text: messageText,
        },
      },
      {
        updateTextStyle: {
          textStyle: { bold: false },
          range: {
            startIndex: startIndex,
            endIndex,
          },
          fields: "bold",
        },
      },
      {
        updateParagraphStyle: {
          paragraphStyle: {
            indentFirstLine: {
              magnitude: 18,
              unit: "PT",
            },
            indentStart: {
              magnitude: 18,
              unit: "PT",
            },
          },
          range: {
            startIndex:
              startIndex + newLineCharacterLength + newLineCharacterLength,
            endIndex,
          },
          fields: "indentFirstLine,indentStart",
        },
      },
    ];

    requests.push(
      ...unreadTagRequests,
      ...fromLabelRequests,
      ...fromContentRequests,
      ...dateLabelRequests,
      ...dateContentRequests,
      ...subjectLabelRequests,
      ...subjectContentRequests,
      ...messageContentRequests
    );
  }
  await updateDoc(documentId, requests);
  logger.debug("Updated doc");
};

const getEmailContents = async (
  page,
  groupedEmails,
  readOnly,
  subject,
  sent,
  currentPage = 1
) => {
  logger.debug(`Downloading emails from page ${currentPage}`);

  const nextPage = currentPage + 1;
  const emailContents = sent
    ? await getSentEmailContentsFromPage(page, readOnly, subject)
    : await getEmailContentsFromPage(page, readOnly, subject);

  logger.debug(
    `Downloaded ${emailContents.count} ${
      emailContents.count > 1 ? "emails" : "email"
    } on page ${currentPage}`
  );

  for (let date in emailContents) {
    if (!Array.isArray(groupedEmails[date])) {
      groupedEmails[date] = [];
    }

    groupedEmails[date].push(...emailContents[date]);
  }

  groupedEmails.count = groupedEmails.count + emailContents.count;

  try {
    logger.debug(`Navigating to page ${nextPage}`);
    await clickNextInboxPageLink(page, nextPage, sent);
  } catch {
    logger.warn(`Unable to navigate to page ${nextPage}`);
    return;
  }

  return getEmailContents(
    page,
    groupedEmails,
    readOnly,
    subject,
    sent,
    nextPage
  );
};

const uploadEmailsToDocs = async (groupedEmails) => {
  const mainDocumentId = process.env.GOOGLE_MAIN_DOC_ID;
  const archiveDocumentId = process.env.GOOGLE_ARCHIVE_DOC_ID;

  const dates = Object.keys(groupedEmails);
  const dateFormat = "m/d/yy";
  const parseDate = (date) => dateFns.parse(date, dateFormat, new Date());
  const sortedDates = dates.sort((left, right) =>
    dateFns.compareAsc(parseDate(left), parseDate(right))
  );

  for (let date of sortedDates) {
    const sortedEmails = groupedEmails[date].reverse();

    if (mainDocumentId) {
      await updateGoogleDoc(mainDocumentId, date, sortedEmails);
    } else {
      logger.debug("No main document ID provided");
    }

    if (archiveDocumentId) {
      await updateGoogleDoc(archiveDocumentId, date, sortedEmails);
    } else {
      logger.debug("No archive document ID provided");
    }
  }
};

exports.downloadEmails = async function downloadEmails({
  startDate,
  endDate,
  readOnly,
  subject,
  sent,
}) {
  try {
    const browser = await puppeteer.launch({ devtools: true });
    const page = await browser.newPage();
    await page.setViewport({ width: 1900, height: 1000 });

    await login(page);

    if (sent) {
      logger.debug("Navigating to Sent Messages page");
      await page.goto("https://www.corrlinks.com/SentMessages.aspx");
    } else {
      logger.debug("Navigating to Inbox page");
      await page.goto("https://www.corrlinks.com/Inbox.aspx");
    }

    try {
      await filterEmailsByDates(page, startDate, endDate);
    } catch (exception) {
      await login(page);

      if (sent) {
        logger.debug("Navigating to Sent Messages page");
        await page.goto("https://www.corrlinks.com/SentMessages.aspx");
      } else {
        logger.debug("Navigating to Inbox page");
        await page.goto("https://www.corrlinks.com/Inbox.aspx");
      }

      await filterEmailsByDates(page, startDate, endDate);
    }

    const groupedEmails = { count: 0 };
    Object.defineProperty(groupedEmails, "count", { enumerable: false });
    await getEmailContents(page, groupedEmails, readOnly, subject, sent);

    if (startDate === endDate) {
      logger.debug(
        `Downloaded ${groupedEmails.count} emails received on ${startDate}`
      );
    } else {
      logger.debug(
        `Downloaded ${groupedEmails.count} emails received ${startDate}-${endDate}`
      );
    }

    await fs.writeFile(
      "./downloaded-emails.json",
      JSON.stringify(groupedEmails, null, 2)
    );

    await uploadEmailsToDocs(groupedEmails);

    logger.debug("Done!");
  } catch (error) {
    logger.error(error);
    // process.exit(1);
  }
};
